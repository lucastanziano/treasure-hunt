#!/bin/bash

mvn clean install

nohup java -jar target/treasure-hunt-1.0-SNAPSHOT.jar server config_prod.yml > logs.txt 2>&1 &