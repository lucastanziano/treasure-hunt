package th;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import th.views.*;
import th.views.home.LandingPage;
import th.views.secp.SecurityPlatform;

public class MainApplication extends Application<MainConfiguration> {

    public static void main(String[] args) throws Exception {
        new MainApplication().run(args);
    }

    @Override
    public String getName() {
        return "treasure-hunt";
    }

    @Override
    public void initialize(Bootstrap<MainConfiguration> bootstrap) {
        bootstrap.addBundle(new ViewBundle<Configuration>());
        bootstrap.addBundle(new AssetsBundle("/assets"));
    }

    @Override
    public void run(MainConfiguration configuration,
                    Environment environment) {
        environment.jersey().register(new GetInChallenge());
        environment.jersey().register(new LandingPage());
        environment.jersey().register(new SecurityPlatform());
        environment.jersey().register(new ReferrerChallenge());
        environment.jersey().register(new HiddenButton());
        environment.jersey().register(new InjectionChallenge());
    }

}