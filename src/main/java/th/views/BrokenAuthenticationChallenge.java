package th.views;

public class BrokenAuthenticationChallenge {

    /**
     * in the page there is an input form. causing a validation error in the landing page will show
     * a fake exception with the history logs of http requests as a message. In the logs you see a sessionID.
     * Reusing the sessionID you can get access to a secured page signed in as an administrator.
     *
     */
}
