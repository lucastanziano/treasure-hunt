package th.views;

import io.dropwizard.views.View;
import org.hibernate.validator.constraints.NotEmpty;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;


@Path("/welcome")
public class GetInChallenge {

    @Context
    protected HttpServletRequest httpRequest;

    @GET
    public View getIn(@QueryParam("loc") @NotEmpty String loc) {
        if (loc.equals("in")) {
            return new View("welcome.ftl") {
            };
        }
        return new View("welcome.ftl") {
        };
    }
}