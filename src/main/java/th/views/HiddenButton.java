package th.views;

import io.dropwizard.views.View;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;

/**
 * in the landing page of this challenge a button has the style property "visibility: hidden"
 * looking at the source code and removing this property will make the button visible. press it to go
 * to solution page.
 */


@Path("/hidden")
public class HiddenButton {

    @Context
    protected HttpServletRequest httpRequest;

    @GET
    public View checkReferer() {

        return new View("hidden_button.ftl") {
        };


    }

}
