package th.views;

import io.dropwizard.views.View;
import org.hibernate.validator.constraints.NotEmpty;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;


@Path("/injection")
public class InjectionChallenge {

    /**
     * the query parameter in this page is used in an SQL query to retrieve the content.
     * Hacking this query to show different results from the default will take to solution
     */

    @Context
    protected HttpServletRequest httpRequest;

    @GET
    public View getIn(@QueryParam("query") @NotEmpty String query) {

        String queryLowercase = query.toLowerCase();
        boolean isInjected = query.contains(" or ") && query.contains("==");

        if (isInjected) {
            return new View("blocked_accounts.ftl") {
            };
        }
        return new View("not_welcome.ftl") {
        };
    }
}