package th.views;

import io.dropwizard.views.View;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;


@Path("/refer_me")
public class ReferrerChallenge {

    @Context
    protected HttpServletRequest httpRequest;

    @GET
    public View checkReferer() {
       String referral = httpRequest.getHeader("Referer");
       if(referral != null && referral.contains("google.com")){
           return new View("welcome.ftl") {
           };
       } else{
           return new View("not_welcome.ftl") {
           };
       }

    }
}