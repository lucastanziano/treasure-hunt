package th.views.home;

import io.dropwizard.views.View;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

@Path("/")
public class LandingPage {


    @GET
    public Object getHome(){
        return new View("home_new.ftl") {
        };
    }

    @GET
    @Path("contact_us")
    public Object getContacts(){
        return new View("contact_us.ftl") {
        };
    }

    @GET
    @Path("robots.txt")
    public String getRobots(){
        return "User-agent: * Disallow: /bank/security/platform";
    }

    @POST
    @Path("contact_us")
    public Object getContactsPost(){
        return new View("contact_us_error.ftl") {
        };
    }

    @GET
    @Path("profile")
    public Object getProfile(@QueryParam("session-id") String sessionID){
        if(sessionID != null && sessionID.equals("1i2s3b4c5")){
            return new View("profile.ftl") {
            };
        }else{
            return new View("empty.ftl"){};
        }

    }

//    public Object landing() {
//        URI uri = UriBuilder.fromUri("/welcome?loc=out").build();
//        return Response.seeOther(uri).build();
//    }
}
