package th.views.secp;


import io.dropwizard.views.View;

public class LoginView extends View {

    private final String memorableQuestion;
    private final String error;

    protected LoginView(String memQuestion, String error) {
        super("login.ftl");
        this.memorableQuestion = memQuestion;
        this.error = error;
    }

    public String getMemorableQuestion() {
        return this.memorableQuestion;
    }

    public String getError(){
        return this.error;
    }
}
