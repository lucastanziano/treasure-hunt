package th.views.secp;

import java.util.Arrays;

public enum OTP {
    INSTANCE;

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private static final long TIMEOUT = 25000;
    String key = "default";
    String solution;
    long genTime = 0;

    public String newOTP(){
//        if(System.currentTimeMillis() - genTime < TIMEOUT){
//            return key;
//        }
        this.genTime = System.currentTimeMillis();
        key = "sortme" + randomAlphaNumeric(150);
        solution = generateSolution(key);
        System.out.println(solution);
        return key;
    }

    private String generateSolution(String original){
        char[] chars = original.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }



    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public boolean isValid(String answer){
        if(System.currentTimeMillis() - genTime > TIMEOUT){
            return false;
        }
        return answer.equals(solution);
    }
}
