package th.views.secp;

public enum Restart {
    INSTANCE;

    boolean isRestarted = false;


    public boolean isRestarted(){
        return isRestarted;
    }

    public void restart(){
        this.isRestarted = true;
    }
}
