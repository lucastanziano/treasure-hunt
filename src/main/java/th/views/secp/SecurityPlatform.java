package th.views.secp;


import io.dropwizard.views.View;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

@Path("/bank/security/platform")
public class SecurityPlatform {

    private final boolean DEBUG = false;

    @GET
    public Object getLogin() {
        if (Session.INSTANCE.isLoggedIn()) {
            if (Restart.INSTANCE.isRestarted()) {
                return new View("secp_home_unlocked.ftl") {
                };
            } else {
                return new View("secp_home.ftl") {
                };
            }

        }
        return new LoginView(OTP.INSTANCE.newOTP(), "");
    }

    @GET
    @Path("/revoke")
    public Object revoke() {
        Session.INSTANCE.revoke();
        URI uri = UriBuilder.fromUri("/bank/security/platform").build();
        return Response.seeOther(uri).build();
    }


    @GET
    @Path("/rreesstartpl")
    public Object restart() {
        Restart.INSTANCE.restart();
        URI uri = UriBuilder.fromUri("/bank/security/platform").build();
        return Response.seeOther(uri).build();
    }

    @GET
    @Path("/profile")
    public Object getProfile() {
        if (Session.INSTANCE.isLoggedIn()) {
            if (Session.INSTANCE.getUser().equals("unruffled87")) {
                return new View("profile.ftl") {
                };
            }
            if (Session.INSTANCE.getUser().equals("ITSecMan69")) {
                return new View("profile_security_manager.ftl") {
                };
            }
        }
        URI uri = UriBuilder.fromUri("/bank/security/platform").build();
        return Response.seeOther(uri).build();
    }

    @GET
    @Path("/ecare/unlockk")
    public Object unlockAccounts() {
        URI uri = UriBuilder.fromUri("/bank/security/platform").build();
        if (Session.INSTANCE.isLoggedIn() || DEBUG) {
            if (Restart.INSTANCE.isRestarted() || DEBUG) {
                Unlock.INSTANCE.unlock();
                uri = UriBuilder.fromUri("/bank/security/platform/ecare").build();

            }
        }

        return Response.seeOther(uri).build();
    }

    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Object search(@FormParam("query") String query) {
        if (Session.INSTANCE.isLoggedIn() && Restart.INSTANCE.isRestarted() && Unlock.INSTANCE.isUnlocked()) {

            if (query != null && query.toLowerCase().contains("confluence")) {
                return new View("confluence.ftl") {
                };
            }
        }
        return new View("search_empty.ftl") {
        };
    }

    @GET
    @Path("/ecare")
    public Object getECare(@QueryParam("filter-id") String query) {

        boolean isInjected = false;

        if (Session.INSTANCE.isLoggedIn() || DEBUG) {
            if (Restart.INSTANCE.isRestarted() || DEBUG) {
                if (Unlock.INSTANCE.isUnlocked()) {
                    return new View("secp_ecare_users_unlocked.ftl") {
                    };
                }
                if (query != null) {
                    String queryLowercase = query.toLowerCase();
                    isInjected = queryLowercase.contains(" or ") && queryLowercase.contains("==");
                } else {
                    URI uri = UriBuilder.fromUri("/bank/security/platform/ecare?filter-id=1671").build();
                    return Response.seeOther(uri).build();
                }
                if (isInjected) {
                    return new View("secp_ecare_injected.ftl") {
                    };
                }

                return new View("secp_ecare.ftl") {
                };

            } else {
                return new View("secp_ecare_blocked.ftl") {
                };
            }
        }
        URI uri = UriBuilder.fromUri("/bank/security/platform").build();
        return Response.seeOther(uri).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Object getLoginPost(@FormParam("username") String username, @FormParam("memorableQuestion") String memorableQuestion, @FormParam("memorableAnswer") String memorableAnswer) {
        if (!username.equals("unruffled87") && !username.equals("ITSecMan69")) {
            return new LoginView(OTP.INSTANCE.newOTP(), "Wrong username");
        }
        if (OTP.INSTANCE.isValid(memorableAnswer) || DEBUG) {
            Session.INSTANCE.newSession(username);
            return getLogin();
        }

        return new LoginView(OTP.INSTANCE.newOTP(), "Wrong memorable answer");
    }
}
