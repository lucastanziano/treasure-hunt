package th.views.secp;

public enum Session {
    INSTANCE;

    boolean loggedIn;

    private static final long TIMEOUT = 60 * 60 * 1000;

    long genTime = 0;
    String user = "";

    public void newSession(String user) {
        this.genTime = System.currentTimeMillis();
        this.user = user;
    }

    public String getUser() {
        return this.user;
    }

    public boolean isLoggedIn() {
        return (System.currentTimeMillis() - genTime < TIMEOUT);
    }

    public void revoke() {
        genTime = 0;
        user = "";
    }
}

