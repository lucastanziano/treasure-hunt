package th.views.secp;

public enum Unlock {

    INSTANCE;


    boolean unlocked = false;

    boolean isUnlocked(){
        return this.unlocked;
    }

    void unlock(){
        this.unlocked = true;

    }

}
