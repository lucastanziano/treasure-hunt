<!-- FOOTER -->
<footer>
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2017 InSecure Banking Corporation &middot; <a href="#" onclick="alert('Privacy? What is it?')" >Privacy</a> &middot; <a href="#" onclick="alert('The bank is not responsible for whatever happens to your money')">Terms</a></p>
</footer>