<!DOCTYPE html>
<html lang="en">
<head>
  <#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
</head>
<body>

<#include "../navbar.ftl">
<br>
<br>
<div class="mx-auto" style="width: 600px;">


<form action="/contact_us" method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Your email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <div class="form-group">
            <label for="exampleTextarea">Your question</label>
            <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
        </div>
    <input class="btn btn-primary" type="submit" value="Submit">
</form>

<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>



