<#include "../header.ftl">
<#include "../navbar.ftl">


<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <img class="media-object"  src="/assets/images/investments.png">
            <div class="caption">
                <h3>New investments ideas</h3>
            </div>
        </div>
        <div class="thumbnail">
            <img class="media-object"  src="/assets/images/security.png">
            <div class="caption">
                <h3>Security all around your money</h3>
            </div>
        </div>
        <div class="thumbnail">
            <img class="media-object" src="/assets/images/employee.jpg">
            <div class="caption">
                <h3>First class support</h3>
            </div>
        </div>
    </div>
</div>

<#include "../footer.ftl">


