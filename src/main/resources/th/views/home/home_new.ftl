<!DOCTYPE html>
<html lang="en">
<head>
  <#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
</head>
<body>

<#include "../navbar.ftl">

<#--<div id="myCarousel" class="carousel slide" data-ride="carousel">-->
<#--<ol class="carousel-indicators">-->
<#--<li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
<#--<li data-target="#myCarousel" data-slide-to="1"></li>-->
<#--<li data-target="#myCarousel" data-slide-to="2"></li>-->
<#--</ol>-->
<#--<div class="carousel-inner" role="listbox">-->
<#--<div class="carousel-item active">-->
<#--<img class="first-slide" src="/assets/images/security.png" alt="First slide">-->
<#--<div class="container">-->
<#--<div class="carousel-caption d-none d-md-block text-left">-->
<#--<h1>Example headline.</h1>-->
<#--<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>-->
<#--<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>-->
<#--</div>-->
<#--</div>-->
<#--</div>-->
<#--<div class="carousel-item">-->
<#--<img class="second-slide" src="/assets/images/investments.png" alt="Second slide">-->
<#--<div class="container">-->
<#--<div class="carousel-caption d-none d-md-block">-->
<#--<h1>Another example headline.</h1>-->
<#--<p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>-->
<#--<p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>-->
<#--</div>-->
<#--</div>-->
<#--</div>-->
<#--</div>-->
<#--<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">-->
<#--<span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
<#--<span class="sr-only">Previous</span>-->
<#--</a>-->
<#--<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">-->
<#--<span class="carousel-control-next-icon" aria-hidden="true"></span>-->
<#--<span class="sr-only">Next</span>-->
<#--</a>-->
<#--</div>-->


<!-- Marketing messaging and featurettes
================================================== -->
<!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container marketing">
<br>
<br>

    <div>
        <span ><h2 class="sub-header"><b>InSecure Banking Corporation</b></h2></span>
    </div>
    <!-- START THE FEATURETTES -->

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Original investment opportunities <span class="text-muted">It'll blow your mind.</span>
            </h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod
                semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
                commodo.</p>
        </div>
        <div class="col-md-5">
            <img class="featurette-image img-fluid mx-auto" src="/assets/images/investments.png"
                 alt="Generic placeholder image">
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7 push-md-5">
            <h2 class="featurette-heading">All secure around your money <span
                    class="text-muted">See for yourself.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod
                semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
                commodo.</p>
        </div>
        <div class="col-md-5 pull-md-7">
            <img class="featurette-image img-fluid mx-auto" src="/assets/images/security.png"
                 alt="Generic placeholder image">
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7">
            <h2 class="featurette-heading">Premium technical support <span class="text-muted">Checkmate.</span></h2>
            <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod
                semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
                commodo.</p>
        </div>
        <div class="col-md-5">
            <img class="featurette-image img-fluid mx-auto" src="/assets/images/employee.jpg"
                 alt="Generic placeholder image">
        </div>
    </div>

    <hr class="featurette-divider">

    <!-- /END THE FEATURETTES -->


    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4">
            <img class="rounded-circle" src="/assets/images/ceo.jpg" alt="Generic placeholder image" width="140"
                 height="140">
            <h2>Our CEO</h2>
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->


<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>



