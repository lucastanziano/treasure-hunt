<!DOCTYPE html>
<html lang="en">
<head>
  <#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
</head>
<body>

<#include "../navbar.ftl">

<br>
<br>
<div class="mx-auto" style="width: 600px;">
    <div class="container">
        <div class="span3 well">
            <center>
                <a href="#aboutModal" data-toggle="modal" data-target="#myModal"><img src="/assets/images/employee.jpg" name="aboutme" width="140" height="140" class="rounded-circle"></a>
                <h3>Unruffled, Tom</h3>
            </center>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="myModalLabel">More About Tom</h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <img src="/assets/images/employee.jpg" name="aboutme" width="140" height="140" border="0" class="rounded-circle"></a>
                            <h3 class="media-heading">Unruffled, Tom <small>UK</small></h3>
                            <span><strong>Skills: </strong></span>
                            <span class="alert alert-info">Nonsense networking</span>
                            <span class="alert alert-success">Confused Coding</span>
                            <span class="alert alert-danger">Weak security</span>
                        </center>
                        <hr>
                        <center>
                            <p class="text-left"><strong>Username: </strong><br>
                                unruffled87</p>
                            <br>
                            <p class="text-left"><strong>Role: </strong><br>
                                Middleware</p>
                            <br>
                            <p class="text-left"><strong>Authorization: </strong><br>
                                Security platform admin</p>
                            <br>
                            <p class="text-left"><strong>Favorite quote: </strong><br>
                                When you don't know where to go, ask the robots.</p>
                            <br>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>



<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>



