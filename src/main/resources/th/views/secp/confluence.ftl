<!DOCTYPE html>
<html lang="en">
<head>
<#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">

    <style>
        @import "http://fonts.googleapis.com/css?family=Roboto:300,400,500,700";

        .container { margin-top: 20px; }
        .mb20 { margin-bottom: 20px; }

        hgroup { padding-left: 15px; border-bottom: 1px solid #ccc; }
        hgroup h1 { font: 500 normal 1.625em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin-top: 0; line-height: 1.15; }
        hgroup h2.lead { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; color: #2a3644; margin: 0; padding-bottom: 10px; }

        .search-result .thumbnail { border-radius: 0 !important; }
        .search-result:first-child { margin-top: 0 !important; }
        .search-result { margin-top: 20px; }
        .search-result .col-md-2 { border-right: 1px dotted #ccc; min-height: 140px; }
        .search-result ul { padding-left: 0 !important; list-style: none;  }
        .search-result ul li { font: 400 normal .85em "Roboto",Arial,Verdana,sans-serif;  line-height: 30px; }
        .search-result ul li i { padding-right: 5px; }
        .search-result .col-md-7 { position: relative; }
        .search-result h3 { font: 500 normal 1.375em "Roboto",Arial,Verdana,sans-serif; margin-top: 0 !important; margin-bottom: 10px !important; }
        .search-result h3 > a, .search-result i { color: #248dc1 !important; }
        .search-result p { font: normal normal 1.125em "Roboto",Arial,Verdana,sans-serif; }
        .search-result span.plus { position: absolute; right: 0; top: 126px; }
        .search-result span.plus a { background-color: #248dc1; padding: 5px 5px 3px 5px; }
        .search-result span.plus a:hover { background-color: #414141; }
        .search-result span.plus a i { color: #fff !important; }
        .search-result span.border { display: block; width: 97%; margin: 0 15px; border-bottom: 1px dotted #ccc; }
    </style>
</head>
<body>

<#include "./secp_navbar.ftl">

<br>
<br>
<div class="container">

    <hgroup class="mb20">
        <h1>Search Results</h1>
        <h2 class="lead"><strong class="text-danger">3</strong> results were found for the search for <strong class="text-danger">confluence</strong></h2>
    </hgroup>

    <section class="col-xs-12 col-sm-6 col-md-12">
        <article class="search-result row">
            <div class="col-xs-12 col-sm-12 col-md-3">
                <a href="#" title="Lorem ipsum" class="thumbnail"><img class="col-md-12" src="/assets/images/lock_image.jpeg" alt="Lorem ipsum" /></a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2">
                <ul class="meta-search">
                    <li><i class="glyphicon glyphicon-calendar"></i> <span>06/05/2017</span></li>
                    <li><i class="glyphicon glyphicon-time"></i> <span>4:28 pm</span></li>
                    <li><i class="glyphicon glyphicon-tags"></i> <span>Security</span></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7 excerpet">
                <h3><a href="#" title="">Procedure to unlock dodgy transactions</a></h3>
                <p>Text to the IT Security Manager the reference ID of the transaction to unlock.</p>
            </div>
            <span class="clearfix borda"></span>
        </article>

        <article class="search-result row">
            <div class="col-xs-12 col-sm-12 col-md-3">
                <a href="#" title="Lorem ipsum" class="thumbnail"><img class="col-md-12" src="/assets/images/winner.jpg" alt="Lorem ipsum" /></a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2">
                <ul class="meta-search">
                    <li><i class="glyphicon glyphicon-calendar"></i> <span>02/04/2017</span></li>
                    <li><i class="glyphicon glyphicon-time"></i> <span>8:32 pm</span></li>
                    <li><i class="glyphicon glyphicon-tags"></i> <span>Competition results</span></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7">
                <h3><a href="#" title="">Last month winner for IT Security Competition!</a></h3>
                <p>The winner for the competition "guess how much money in coins the IT Security Manager keeps in his lock box" was the user <i>ITSecMan69</i> for the 6th time in a row!</p>
            </div>
            <span class="clearfix borda"></span>
        </article>

        <article class="search-result row">
            <div class="col-xs-12 col-sm-12 col-md-3">
                <a href="#" title="Lorem ipsum" class="thumbnail"><img class="col-md-12" src="/assets/images/win_cash.jpg" alt="Lorem ipsum" /></a>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2">
                <ul class="meta-search">
                    <li><i class="glyphicon glyphicon-calendar"></i> <span>01/5/2017</span></li>
                    <li><i class="glyphicon glyphicon-time"></i> <span>10:13 am</span></li>
                    <li><i class="glyphicon glyphicon-tags"></i> <span>Competitions</span></li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-7">
                <h3><a href="#" title="">[MAY] NEW COMPETITION! WIN MONEY!</a></h3>
                <p>300£ prize to whom can guess how much money in coins the IT Security Manager keeps stored in his lock box. Text him your guess... if you can find the mobile number first.</p>
            </div>
            <span class="clearfix border"></span>
        </article>

    </section>



<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>
