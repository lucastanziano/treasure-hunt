<#-- @ftlvariable name="" type="th.views.secp.LoginView" -->
<!DOCTYPE html>
<html lang="en">
<head>
  <#include "../header.ftl">
      <meta http-equiv="refresh" content="25" >
    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
</head>
<body>

<#include "./secp_navbar.ftl">

<br>
<br>
<div class="container">

    <form action="/bank/security/platform" method="post" >
        <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" class="form-control" name="username" aria-describedby="emailHelp" placeholder="Enter username">
        </div>

        <div class="form-group">
            <label for="exampleTextarea">One Time Memorable Question</label>
            <textarea class="form-control" name="memorableQuestion" rows="3">${memorableQuestion?html}</textarea>
            <small id="emailHelp" class="form-text text-muted">It will change every 15 seconds.</small>
        </div>
        <div class="form-group">
            <label for="exampleTextarea">One Time Memorable Answer</label>
            <textarea class="form-control" name="memorableAnswer" rows="3"></textarea>
        </div>
        <input class="btn btn-primary" type="submit" value="Login">
    </form>
    <div class="alert alert-danger" role="alert">
            ${error?html}
    </div>

<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>



