<!DOCTYPE html>
<html lang="en">
<head>
  <#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
</head>
<body>

<#include "./secp_navbar.ftl">

<br>
<br>
<div class="mx-auto" style="width: 600px;">
    <div class="container">
        <div class="span3 well">
            <center>
                <a href="#aboutModal" data-toggle="modal" data-target="#myModal"><img src="/assets/images/security_manager.jpg" name="aboutme" width="140" height="140" class="rounded-circle"></a>

                <h3>Fluffy, Harnold</h3>
            </center>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="myModalLabel">More About Harnold</h4>
                    </div>
                    <div class="modal-body">
                        <center>
                            <img src="/assets/images/security_manager.jpg" name="aboutme" width="140" height="140" border="0" class="rounded-circle"></a>
                            <h3 class="media-heading">Fluffy, Harnold <small>UK</small></h3>
                            <span><strong>Skills: </strong></span>
                            <span class="alert alert-info">Managing ants</span>
                            <span class="alert alert-success">Eating donuts</span>
                            <span class="alert alert-danger">Secure lock boxes</span>
                        </center>
                        <hr>
                        <center>
                            <p class="text-left"><strong>Username: </strong><br>
                                ITSecMan69</p>
                            <br>
                            <p class="text-left"><strong>Role: </strong><br>
                                IT Security Manager</p>
                            <br>
                            <p class="text-left"><strong>Authorization: </strong><br>
                                Super Mega Security Platform Admin</p>
                            <br>
                            <p class="text-left"><strong>Favorite quote: </strong><br>
                                Lock box combination: 065</p>
                            <br>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>



<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>



