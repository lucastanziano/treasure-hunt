<!DOCTYPE html>
<html lang="en">
<head>
<#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
    <link href="/assets/secp_home.css" rel="stylesheet">
</head>
<body>

<#include "./secp_navbar.ftl">

<br>
<br>
<div class="container marketing">


    <div class="page-heading">
        <h1>(no on)eCare</h1>
    </div>

    <br>
    <form class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Search" value="1671"></input>
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Event</th>
                <th>Customer</th>
                <th>Details</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1671</td>
                <td>Account Vanished</td>
                <td>Unlucky Jim</td>
                <td>Unlikely event made account vanish</td>
                <td><a href="#">None</a></td>
            </tr>


            </tbody>
        </table>
    </div>

<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>
