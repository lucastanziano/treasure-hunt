<!DOCTYPE html>
<html lang="en">
<head>
<#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
    <link href="/assets/secp_home.css" rel="stylesheet">
</head>
<body>

<#include "./secp_navbar.ftl">

<br>
<br>
<div class="container marketing">


    <div class="page-heading">
        <h1>(no on)eCare</h1>
    </div>

    <br>
    <div></div>
    <h2 class="sub-header">Most recent events</h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Event</th>
                <th>Customer</th>
                <th>Details</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1670</td>
                <td>Account Locked</td>
                <td>Luca Stanziano</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1672</td>
                <td>Account Locked</td>
                <td>Raul Rodrigez</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1673</td>
                <td>Account Locked</td>
                <td>Natalia Oskina</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1674</td>
                <td>Account Locked</td>
                <td>Yurena Cabrera</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1675</td>
                <td>Account Locked</td>
                <td>Ina Hristova</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1676</td>
                <td>Account Locked</td>
                <td>Sepp Renfer</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1677</td>
                <td>Account Locked</td>
                <td>My-Yen Renfer</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1678</td>
                <td>Account Locked</td>
                <td>Ole Hildebrandt</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1679</td>
                <td>Account Locked</td>
                <td>Ana Hildebrandt</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1680</td>
                <td>Account Locked</td>
                <td>Huw Daves</td>
                <td>Dodgy transaction</td>
                <td><a href="/bank/security/platform/ecare/unlockk">Unlock</a></td>
            </tr>
            <tr>
                <td>1681</td>
                <td>Account Locked</td>
                <td>Chris Fey</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1682</td>
                <td>Account Locked</td>
                <td>Benjamin Dorig</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1683</td>
                <td>Account Locked</td>
                <td>James Every</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>

            </tbody>
        </table>
    </div>

<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>



