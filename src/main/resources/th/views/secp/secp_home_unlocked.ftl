<!DOCTYPE html>
<html lang="en">
<head>
<#include "../header.ftl">

    <!-- Custom styles for this template -->
    <link href="/assets/home.css" rel="stylesheet">
    <link href="/assets/secp_home.css" rel="stylesheet">
</head>
<body>

<#include "./secp_navbar.ftl">

<br>
<br>
<div class="container marketing">
    <div class="row">
        <div class="col-md-5">
            <a href="#" onclick="alert('The platform is already running')" class="btn  btn-success btn-lg"><i class="glyphicon glyphicon-play"></i> <strong>Start</strong></a>
            <a href="#" onclick="alert('You shouldn\'t stop a service that is properly running..it\'s a rare event!')" class="btn  btn-warning btn-lg"><i class="glyphicon glyphicon-stop"></i> <strong>Stop</strong></a>
            <a href="/bank/security/platform/" style="visibility: hidden" class="btn  btn-danger btn-lg"><i class="glyphicon glyphicon-repeat"></i> <strong>Restart</strong></a>
        </div>
    </div>
    <div class="page-heading">
        <h1>Dashboard</h1>
    </div>

    <div class="row">
        <div class="col-md-3">
            <a class="info-tiles tiles-danger has-footer" href="#">
                <div class="tiles-heading">
                    <div class="pull-left">Security violations</div>
                    <div class="pull-right">
                        <div id="tileorders" class="sparkline-block">
                            <canvas width="39" height="13"
                                    style="display: inline-block; width: 39px; height: 13px; vertical-align: top;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tiles-body">
                    <div class="text-center">1,275</div>
                </div>
                <div class="tiles-footer">
                    <div class="pull-right percent-change">+20.7%</div>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a class="info-tiles tiles-orange has-footer" href="#">
                <div class="tiles-heading">
                    <div class="pull-left">Loss</div>
                    <div class="pull-right">
                        <div id="tilerevenues" class="sparkline-block">
                            <canvas width="40" height="13"
                                    style="display: inline-block; width: 40px; height: 13px; vertical-align: top;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tiles-body">
                    <div class="text-center">£71,2 mil</div>
                </div>
                <div class="tiles-footer">
                    <div class="pull-right percent-change">+17.2%</div>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a class="info-tiles tiles-blue has-footer" href="#">
                <div class="tiles-heading">
                    <div class="pull-left">IT Support Tickets</div>
                    <div class="pull-right">
                        <div id="tiletickets" class="sparkline-block">
                            <canvas width="13" height="13"
                                    style="display: inline-block; width: 13px; height: 13px; vertical-align: top;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tiles-body">
                    <div class="text-center">1,500</div>
                </div>
                <div class="tiles-footer">
                    <div class="pull-right percent-change">+10.3%</div>
                </div>
            </a>
        </div>

        <div class="col-md-3">
            <a class="info-tiles tiles-magenta has-footer" href="#">
                <div class="tiles-heading">
                    <div class="pull-left">Critical errors</div>
                    <div class="pull-right">
                        <div id="tilemembers" class="sparkline-block">
                            <canvas width="39" height="13"
                                    style="display: inline-block; width: 39px; height: 13px; vertical-align: top;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="tiles-body">
                    <div class="text-center">6,500</div>
                </div>
                <div class="tiles-footer">
                    <div class="pull-right percent-change">-11.1%</div>
                </div>
            </a>
        </div>


    </div>


    <!-- END STATS -->
    <br>

    <br>
    <h2 class="sub-header">Most recent events</h2>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Event</th>
                <th>Customer</th>
                <th>Details</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1670</td>
                <td>Account Locked</td>
                <td>Luca Stanziano</td>
                <td>Dodgy transaction</td>
                <td><a href="" onclick="alert('Go to eCare to unlock')">Unlock</a></td>
            </tr>
            <tr>
                <td>1672</td>
                <td>Account Locked</td>
                <td>Raul Rodrigez</td>
                <td>Dodgy transaction</td>
                <td><a href="" onclick="alert('Go to eCare to unlock')">Unlock</a></td>
            </tr>
            <tr>
                <td>1673</td>
                <td>Account Locked</td>
                <td>Natalia Oskina</td>
                <td>Dodgy transaction</td>
                <td><a href="" onclick="alert('Go to eCare to unlock')">Unlock</a></td>
            </tr>
            <tr>
                <td>1674</td>
                <td>Account Locked</td>
                <td>Yurena Cabrera</td>
                <td>Dodgy transaction</td>
                <td><a href="" onclick="alert('Go to eCare to unlock')">Unlock</a></td>
            </tr>
            <tr>
                <td>1675</td>
                <td>Account Locked</td>
                <td>Ina Hristova</td>
                <td>Dodgy transaction</td>
                <td><a href="" onclick="alert('Go to eCare to unlock')">Unlock</a></td>
            </tr>
            <tr>
                <td>1676</td>
                <td>Account Locked</td>
                <td>Sepp Renfer</td>
                <td>Dodgy transaction</td>
                <td><a href="" onclick="alert('Go to eCare to unlock')">Unlock</a></td>
            </tr>
            <tr>
                <td>1677</td>
                <td>Account Locked</td>
                <td>My-Yen Renfer</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1678</td>
                <td>Account Locked</td>
                <td>Ole Hildebrandt</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1679</td>
                <td>Account Locked</td>
                <td>Ana Hildebrandt</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1680</td>
                <td>Account Locked</td>
                <td>Huw Daves</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1681</td>
                <td>Account Locked</td>
                <td>Chris Fey</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1682</td>
                <td>Account Locked</td>
                <td>Benjamin Dorig</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            <tr>
                <td>1683</td>
                <td>Account Locked</td>
                <td>James Every</td>
                <td>Dodgy transaction</td>
                <td><a href="">Unlock</a></td>
            </tr>
            </tbody>
        </table>
    </div>
<#include "../footer.ftl">

</div><!-- /.container -->


<#include "../common_scripts.ftl">
</body>
</html>



