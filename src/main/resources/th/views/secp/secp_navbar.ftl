<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/">ISBC</a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/bank/security/platform">Security Platform <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bank/security/platform/ecare">eCare</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bank/security/platform/profile">Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/bank/security/platform/revoke">Sign out</a>
            </li>
        </ul>
        <form class="form-inline mt-2 mt-md-0" action="/bank/security/platform/search" method="post">
            <input class="form-control mr-sm-2" type="text" name="query" placeholder="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>